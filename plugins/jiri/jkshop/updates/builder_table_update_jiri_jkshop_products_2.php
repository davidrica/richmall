<?php namespace Jiri\JKShop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJiriJkshopProducts2 extends Migration
{
    public function up()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
