<?php namespace Jiri\JKShop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJiriJkshopProducts extends Migration
{
    public function up()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->dropPrimary(['id']);
            $table->dropColumn('id');
        });
    }
    
    public function down()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->integer('id')->unsigned();
            $table->primary(['id']);
        });
    }
}
