<?php namespace Richmall\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRichmallHomepage extends Migration
{
    public function up()
    {
        Schema::create('richmall_homepage_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('richmall_homepage_');
    }
}
