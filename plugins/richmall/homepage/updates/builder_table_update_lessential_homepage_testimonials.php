<?php namespace Richmall\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRichmallHomepageTestimonials extends Migration
{
    public function up()
    {
        Schema::table('richmall_homepage_testimonials', function($table)
        {
            $table->text('testimonial')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('richmall_homepage_testimonials', function($table)
        {
            $table->smallInteger('testimonial')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
