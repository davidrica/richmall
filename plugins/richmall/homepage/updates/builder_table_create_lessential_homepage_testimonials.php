<?php namespace Richmall\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRichmallHomepageTestimonials extends Migration
{
    public function up()
    {
        Schema::create('richmall_homepage_testimonials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->smallInteger('testimonial');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('richmall_homepage_testimonials');
    }
}
