<?php namespace Richmall\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteRichmallHomepage extends Migration
{
    public function up()
    {
        Schema::dropIfExists('richmall_homepage_');
    }
    
    public function down()
    {
        Schema::create('richmall_homepage_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
}
