<?php namespace Richmall\Homepage;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerNavigation() {
        return [
            'homepage' => [
                'label' => 'Homepage',
                'url' => Backend::url('richmall/homepage/sliders'),
                'icon' => 'icon-home',
                'permissions' => ['richmall.homepage.*'],
                'order' => 69,
                'sideMenu' => [
                    'sliders' => [
                        'label' => 'Sliders',
                        'icon' => 'icon-desktop',
                        'url' => Backend::url('richmall/homepage/sliders'),
                        'permissions' => ['richmall.homepage.sliders'],
                    ],
                    'testimonials' => [
                        'label' => 'Testimonials',
                        'icon' => 'icon-users',
                        'url' => Backend::url('richmall/homepage/testimonials'),
                        'permissions' => ['richmall.homepage.testimonials'],
                    ]
                ]
            ]
        ];
    }

    public function registerSettings()
    {
    }
}
