<?php namespace Richmall\Homepage\Models;

use Model;

/**
 * Model
 */
class Testimonials extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'richmall_homepage_testimonials';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'avatar' => 'System\Models\File'
    ];
}
