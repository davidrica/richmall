let elixir = require('laravel-elixir');
require('laravel-elixir-livereload');

elixir.config.assetsPath = 'themes/richmall/assets/';
elixir.config.publicPath = 'themes/richmall/assets/compiled/';

elixir(function(mix){

    mix.sass('style.scss');

    mix.scripts([
        'jquery.js',
        'uikit.js',
        'uikit-icons.js',
        'semantic/transition.min.js',
        'semantic/popup.min.js',
        'slick.js',
        'app.js'
    ]);

    mix.livereload([
        'themes/richmall/assets/compiled/css/style.css',
        'themes/richmall/**/*.htm',
        'themes/richmall/assets/compiled/js/*.js'
    ])

});
